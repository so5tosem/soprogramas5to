/*Equipo: 
Beckham Martinez Nayeli
Espinoza Rosas Eduardo
Rascon Beltran Adalberto
Rivas Dummer German
*/

package proyectoso;

import java.io.IOException;
import javax.swing.UIManager;

public class ProyectoSO {

    public static void main(String[] args) throws IOException {
        try{
             UIManager.setLookAndFeel("ch.randelshofer.quaqua.QuaquaLookAndFeel");
         }catch(Exception e){
         }
        Ventana vta = new Ventana();
        vta.setVisible(true);
    }
    
}
