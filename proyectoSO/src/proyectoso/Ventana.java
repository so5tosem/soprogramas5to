package proyectoso;

import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.DefaultTableCellRenderer;

public class Ventana extends JFrame implements Runnable{
    JPanel panel = new JPanel();
    JPanel cuadrante1 = new JPanel();
    JPanel cuadrante2 = new JPanel();
    JPanel cuadrante3 = new JPanel();
    JPanel cuadrante4 = new JPanel();
    DefaultTableModel tablaP;
    DefaultTableCellRenderer renderer;
    //JPanel reloj = new JPanel();
    
    int hr,min,seg;
    Calendar calendario = new GregorianCalendar();
    Thread hiloHora;
    JLabel relojLabel = new JLabel();
    Font fuenteReloj = null;
    Font header = null;
    Font celdas = null;
    String fuenteReloj2 = "/font/DS-DIGIT.TTF"; 
    String fuenteHeader = "/font/Overlock-Black.ttf";
    String fuenteCeldas = "/font/Overlock-Regular.ttf";
    JButton btnM, btnM2;
    String[] fila1 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila2 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila3 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila4 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila5 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila6 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila7 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila8 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila9 = {"","","","","","","","","","","","","","","","","",""};
    String[] fila10 = {"","","","","","","","","","","","","","","","","",""};
    AudioClip clip;
    URL url;
   
    
    public Ventana() throws IOException{
         
        hiloHora = new Thread(this); 
        hiloHora.start();
        
        setTitle("Procesos");
        this.setExtendedState(MAXIMIZED_BOTH);
       
        panel.setBackground(Color.black);
        this.getContentPane().add(panel);
        panel.setLayout(new GridLayout(2,2,8,8));
        panel.add(cuadrante1);
        panel.add(cuadrante2);
        panel.add(cuadrante3);
        panel.add(cuadrante4);
        JSeparator separatorc1 = new JSeparator(SwingConstants.HORIZONTAL);
        separatorc1.setBounds(0, 343, 700, 500);
        cuadrante1.add(separatorc1);
        JSeparator separatorc1v = new JSeparator(SwingConstants.VERTICAL);
        separatorc1v.setBounds(675, 0, 700, 500);
        cuadrante1.add(separatorc1v);
        
        JSeparator separatorc2 = new JSeparator(SwingConstants.HORIZONTAL);
        separatorc2.setBounds(0, 343, 700, 500);
        cuadrante2.add(separatorc2);
        JSeparator separatorc2v = new JSeparator(SwingConstants.VERTICAL);
        separatorc2v.setBounds(0, 0, 700, 500);
        cuadrante2.add(separatorc2v);
        
        JSeparator separatorc3 = new JSeparator(SwingConstants.HORIZONTAL);
        separatorc3.setBounds(0, 0, 700, 500);
        cuadrante3.add(separatorc3);
        JSeparator separatorc3v = new JSeparator(SwingConstants.VERTICAL);
        separatorc3v.setBounds(675, 0, 700, 500);
        cuadrante3.add(separatorc3v);
        
        JSeparator separatorc4 = new JSeparator(SwingConstants.HORIZONTAL);
        separatorc4.setBounds(0, 0, 700, 500);
        cuadrante4.add(separatorc4);
        JSeparator separatorc4v = new JSeparator(SwingConstants.VERTICAL);
        separatorc4v.setBounds(0, 0, 700, 500);
        cuadrante4.add(separatorc4v);
        //this.url = Ventana.class.getResource("/rolas/rolon.wav");
        //this.clip = Applet.newAudioClip(url);
        tablaProcesos();        
        reloj();
        botones();
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
    }
    
    public void tablaProcesos(){
        JLabel etq = new JLabel();
        tablaP  = new DefaultTableModel();
        tablaP.addColumn("ID");
        tablaP.addColumn("HIP");
        tablaP.addColumn("TTP");
        tablaP.addColumn("QUANTUM");
        tablaP.addColumn("PRI");
        tablaP.addColumn("STATUS");
        tablaP.addColumn("SUC1");
        tablaP.addColumn("HISUC1");
        tablaP.addColumn("TTSUC1");
        tablaP.addColumn("SUC2");
        tablaP.addColumn("HISUC2");
        tablaP.addColumn("TTSUC2");
        tablaP.addColumn("HIBS");
        tablaP.addColumn("TTBS");
        tablaP.addColumn("HILS");
        tablaP.addColumn("TTLS");
        tablaP.addColumn("RM");
        tablaP.addRow(fila1);
        tablaP.addRow(fila2);
        tablaP.addRow(fila3);
        tablaP.addRow(fila4);
        tablaP.addRow(fila5);
        tablaP.addRow(fila6);
        tablaP.addRow(fila7);
        tablaP.addRow(fila8);
        tablaP.addRow(fila9);
        tablaP.addRow(fila10);
        /*tablaP.addRow(fila11);
        tablaP.addRow(fila12);
        tablaP.addRow(fila13);
        tablaP.addRow(fila14);
        tablaP.addRow(fila15);
        tablaP.addRow(fila16);
        tablaP.addRow(fila17);
        tablaP.addRow(fila18);
        tablaP.addRow(fila19);
        tablaP.addRow(fila20);
        tablaP.addRow(fila21);
        tablaP.addRow(fila22);
        tablaP.addRow(fila23);
        */
       
      
        JTable tablaProcesos = new JTable(tablaP);
        tablaProcesos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        renderer = (DefaultTableCellRenderer)
        tablaProcesos.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);
        tablaProcesos.getTableHeader().setFont(fuenteHeader(fuenteHeader, 1, 14));
        tablaProcesos.getTableHeader().setForeground(new Color(146, 14, 14));
        
        tablaProcesos.getTableHeader().setBackground(Color.GRAY);
        tablaProcesos.setFont(fuenteCeldas(fuenteCeldas, 1, 12));
        JScrollPane scroll = new JScrollPane(tablaProcesos);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setBounds(40,80,600,200);
        tablaProcesos.setBounds(40,80,600,200);    
        
        etq.setText("TABLA DE PROCESOS");
        etq.setFont(fuenteHeader(fuenteHeader,1,18));
        etq.setForeground(new Color(0,0,204));
        etq.setBounds(255, 10, 200, 50);
        etq.setVisible(true);
        
        cuadrante1.add(etq);
        cuadrante1.add(scroll);
        cuadrante1.setLayout(null);
        cuadrante1.setVisible(true);        
        cuadrante2.setLayout(null);
        cuadrante2.setVisible(true);        
        cuadrante3.setLayout(null);
        cuadrante3.setVisible(true);        
        cuadrante4.setLayout(null);
        cuadrante4.setVisible(true);        
    }
    
    public void reloj() {   
        Date fecha = new Date();
        calendario.setTime(fecha);
        hr = calendario.get(Calendar.HOUR_OF_DAY);
        min = calendario.get(Calendar.MINUTE);
        seg = calendario.get(Calendar.SECOND);
        
        relojLabel.setForeground(new Color(31,137,19));
        relojLabel.setFont(fuenteReloj(fuenteReloj2, 1, 25));
        relojLabel.setBounds(580, 280, 100, 100);
        relojLabel.setVisible(true);
        cuadrante1.add(relojLabel);
        cuadrante1.setLayout(null);
        cuadrante1.setVisible(true);
    }
    
    public Font fuenteReloj(String fuente, int estilo, int tamaño){
        try {
            InputStream stream =  getClass().getResourceAsStream(fuente);
            fuenteReloj = Font.createFont(Font.TRUETYPE_FONT, stream);
        } catch (Exception ex) {
            fuenteReloj = new Font("Arial", Font.BOLD, 15);            
        }
        Font fuenteFinal = fuenteReloj.deriveFont(estilo, tamaño);
        return fuenteFinal;
    }
    
    public Font fuenteHeader(String fuente, int estilo, int tamaño){
        try {
            InputStream stream =  getClass().getResourceAsStream(fuente);
            header = Font.createFont(Font.TRUETYPE_FONT, stream);
        } catch (Exception ex) {
            header = new Font("Arial", Font.BOLD, 15);            
        }
        Font fuenteFinal = header.deriveFont(estilo, tamaño);
        return fuenteFinal;
    }

    public Font fuenteCeldas(String fuente, int estilo, int tamaño){
        try {
            InputStream stream =  getClass().getResourceAsStream(fuente);
            celdas = Font.createFont(Font.TRUETYPE_FONT, stream);
        } catch (Exception ex) {
            celdas = new Font("Arial", Font.BOLD, 15);            
        }
        Font fuenteFinal = celdas.deriveFont(estilo, tamaño);
        return fuenteFinal;
    }


    @Override
    public void run() {
        Thread hilo = Thread.currentThread();
        while (hilo == hiloHora) {
            reloj();
            relojLabel.setText(hr + ":" + min + ":" + seg); 
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }            
        }
    }
    
    public void botones()
    {
        btnM = new JButton("",new ImageIcon(getClass().getResource("/imagenes/button_isF.png")));
        btnM.setOpaque(false);
        btnM.setContentAreaFilled(false);
        btnM.setBorderPainted(false);
        btnM.setBounds(420, 270, 70, 45);
        btnM.setVisible(true);
        JButton btnI = new JButton("",new ImageIcon(getClass().getResource("/imagenes/button_isC.png")));
        btnI.setOpaque(false);
        btnI.setContentAreaFilled(false);
        btnI.setBorderPainted(false);
        btnI.setBounds(500, 270, 70, 45);
        btnI.setVisible(true);
        cuadrante4.add(btnM);
        cuadrante4.add(btnI);
        cuadrante4.setLayout(null);
        cuadrante4.setVisible(true);
        btnM.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {  
                tablaP.removeRow(0);
                tablaP.removeRow(0);
                tablaP.removeRow(0);
                tablaP.removeRow(0);
                tablaP.removeRow(0);
                tablaP.removeRow(0);
                tablaP.removeRow(0);
                tablaP.removeRow(0);
                tablaP.removeRow(0);
                tablaP.removeRow(0);
//                clip.loop();
                int hrA = hr, minA = min+2, segA = seg;
                int hrA1 = hr, minA1 = min+2, segA1 = seg;
                fila1[0] = "001";
                String v1 = hr + ":" + (min+2) + ":" + seg;                
                fila1[1] = v1;
                String f12 = (int)(Math.random()*60+60) + "";
                fila1[2] = f12;
                fila1[3] = 5 + "";
                String f14 = (int)(Math.random()*11) + "";
                fila1[4] = f14;       
                String status = "Ejecución";
                fila1[5] = status;              
                String f16 = ((int)(Math.random()*4+2)*10) + "";
                fila1[6] = f16;
                int hora = (int)(Math.random()*30+31);  
                if(hora + seg >= 60)
                {
                    minA+=1;
                    segA = (hora + seg) - 60;
                    if(minA >= 60)
                    {
                        hrA+=1;
                        minA = 0;
                    }
                }
                else                                   
                    segA = segA + hora;
                String f17 = hrA + ":" + minA + ":" + segA;  
                fila1[7] = f17;
                String f18 = (int)(Math.random()*20+20) + "";                
                fila1[8] = f18;
                String f19 = ((int)(Math.random()*4+3)*10) + "";
                fila1[9] = f19;
                hora = (int)(Math.random()*30+30);
                if(hora + segA >= 60)
                {
                    minA+=1;
                    segA = (hora + segA) - 60;
                    if(minA >= 60)
                    {
                        hrA+=1;
                        minA = 0;
                    }
                }
                else
                    segA = segA + hora;               
                hora = Integer.parseInt(f18);
                if(hora + segA >= 60)
                {
                    minA+=1;
                    segA = (hora + segA) - 60;
                    if(minA >= 60)
                    {
                        hrA+=1;
                        minA = 0;
                    }
                }
                else
                    segA += hora;
                String f110 = hrA + ":" + minA + ":" + segA;  
                fila1[10] = f110;
                String f111 = (int)(Math.random()*20+21) + "";
                fila1[11] = f111;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 1 HIBS: " + hora);
                if(hora + segA >= 60)
                {
                    minA+=1;
                    segA = (hora + segA) - 60;
                    if(minA >= 60)
                    {
                        hrA+=1;
                        minA = 0;
                    }
                }
                else
                    segA = segA + hora;               
                hora = Integer.parseInt(f111);
                if(hora + segA >= 60)
                {
                    minA+=1;
                    segA = (hora + segA) - 60;
                    if(minA >= 60)
                    {
                        hrA+=1;
                        minA = 0;
                    }
                }
                else
                    segA += hora;
                String f112 = hrA + ":" + minA + ":" + segA;                
                fila1[12] = f112;
                String f113 = (int)(Math.random()*20+21) + "";
                fila1[13] = f113;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 1 HILS: " + hora);
                if(hora + segA >= 60)
                {
                    minA+=1;
                    segA = (hora + segA) - 60;
                    if(minA >= 60)
                    {
                        hrA+=1;
                        minA = 0;
                    }
                }
                else
                    segA = segA + hora;               
                hora = Integer.parseInt(f113);
                if(hora + segA >= 60)
                {
                    minA+=1;
                    segA = (hora + segA) - 60;
                    if(minA >= 60)
                    {
                        hrA+=1;
                        minA = 0;
                    }
                }
                else
                    segA += hora;
                String f114 = hrA + ":" + minA + ":" + segA;    
                fila1[14] = f114;
                String f115 = (int)(Math.random()*20+21) + "";
                fila1[15] = f115;                
                tablaP.insertRow(0, fila1); 
                
                
                fila2[0] = "002";
                hora = (int)(Math.random()*16+30);
                //System.out.println(hora);
                if(hora + segA1 >= 60)
                {
                    minA1+=1;
                    segA1 = (hora + segA1) - 60;
                    if(minA1 >= 60)
                    {
                        hrA1+=1;
                        minA1 = 0;
                    }
                }
                else
                    segA1 += hora;
                int hrA2 = hrA1, minA2 = minA1, segA2 = segA1;
                String f21 = hrA1 + ":" + minA1 + ":" + segA1;  
                fila2[1] = f21;
                String f22 = (int)(Math.random()*60+60) + "";
                fila2[2] = f22;
                fila2[3] = 5 + "";
                String f24 = (int)(Math.random()*11) + "";
                fila2[4] = f24;                
                fila2[5] = "Ejecución";
                String f26 = ((int)(Math.random()*4+2)*10) + "";
                fila2[6] = f26;
                hora = (int)(Math.random()*30+30);       
//                System.out.println("proceso 2: " + hora);
                if(hora + segA1 >= 60)
                {
                    minA1+=1;
                    segA1 = (hora + segA1) - 60;
                    if(minA1 >= 60)
                    {
                        hrA1+=1;
                        minA1 = 0;
                    }
                }
                else                                   
                    segA1 = segA1 + hora;
                String f27 = hrA1 + ":" + minA1 + ":" + segA1;  
                fila2[7] = f27;
                String f28 = (int)(Math.random()*20+20) + "";                
                fila2[8] = f28;
                String f29 = ((int)(Math.random()*4+3)*10) + "";
                fila2[9] = f29;
                hora = (int)(Math.random()*30+30);                
                if(hora + segA1 >= 60)
                {
                    minA1+=1;
                    segA1 = (hora + segA1) - 60;
                    if(minA1 >= 60)
                    {
                        hrA1+=1;
                        minA1 = 0;
                    }
                }
                else
                    segA1 = segA1 + hora;
                hora = Integer.parseInt(f28);
                if(hora + segA1 >= 60)
                {
                    minA1+=1;
                    segA1 = (hora + segA1) - 60;
                    if(minA1 >= 60)
                    {
                        hrA1+=1;
                        minA1 = 0;
                    }
                }
                else
                    segA1 += hora;
                String f210 = hrA1 + ":" + minA1 + ":" + segA1;  
                fila2[10] = f210;
                String f211 = (int)(Math.random()*20+21) + "";
                fila2[11] = f211;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 2 HIBS: " + hora);
                if(hora + segA1 >= 60)
                {
                    minA1+=1;
                    segA1 = (hora + segA1) - 60;
                    if(minA1 >= 60)
                    {
                        hrA1+=1;
                        minA1 = 0;
                    }
                }
                else
                    segA1 = segA1 + hora;
                hora = Integer.parseInt(f211);
                if(hora + segA1 >= 60)
                {
                    minA1+=1;
                    segA1 = (hora + segA1) - 60;
                    if(minA1 >= 60)
                    {
                        hrA1+=1;
                        minA1 = 0;
                    }
                }
                else
                    segA1 += hora;
                String f212 = hrA1 + ":" + minA1 + ":" + segA1;            
                fila2[12] = f212;
                String f213 = (int)(Math.random()*20+21) + "";
                fila2[13] = f213;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 2 HILS: " + hora);
                if(hora + segA1 >= 60)
                {
                    minA1+=1;
                    segA1 = (hora + segA1) - 60;
                    if(minA1 >= 60)
                    {
                        hrA1+=1;
                        minA1 = 0;
                    }
                }
                else
                    segA1 = segA1 + hora;
                hora = Integer.parseInt(f213);
                if(hora + segA1 >= 60)
                {
                    minA1+=1;
                    segA1 = (hora + segA1) - 60;
                    if(minA1 >= 60)
                    {
                        hrA1+=1;
                        minA1 = 0;
                    }
                }
                else
                    segA1 += hora;
                String f214 = hrA1 + ":" + minA1 + ":" + segA1;  
                fila2[14] = f214;
                String f215 = (int)(Math.random()*20+21) + "";
                fila2[15] = f215;
                tablaP.insertRow(1, fila2);
                
                
                fila3[0] = "003";
                hora = (int)(Math.random()*16+30);
                //System.out.println(hora);
                if(hora + segA2 >= 60)
                {
                    minA2+=1;
                    segA2 = (hora + segA2) - 60;
                    if(minA2 >= 60)
                    {
                        hrA2+=1;
                        minA2 = 0;
                    }
                }
                else
                    segA2 += hora;
                int hrA3 = hrA2, minA3 = minA2, segA3 = segA2;
                String f31 = hrA2 + ":" + minA2 + ":" + segA2;  
                fila3[1] = f31;
                String f32 = (int)(Math.random()*60+60) + "";
                fila3[2] = f32;
                fila3[3] = 5 + "";
                String f34 = (int)(Math.random()*11) + "";
                fila3[4] = f34;                
                fila3[5] = "Ejecución";
                String f36 = ((int)(Math.random()*4+2)*10) + "";
                fila3[6] = f36;                
                hora = (int)(Math.random()*30+30);   
                if(hora + segA2 >= 60)
                {
                    minA2+=1;
                    segA2 = (hora + segA2) - 60;
                    if(minA2 >= 60)
                    {
                        hrA2+=1;
                        minA2 = 0;
                    }
                }
                else                                   
                    segA2 = segA2 + hora;
                String f37 = hrA2 + ":" + minA2 + ":" + segA2;  
                fila3[7] = f37;
                String f38 = (int)(Math.random()*20+20) + "";                
                fila3[8] = f38;
                String f39 = ((int)(Math.random()*4+3)*10) + "";
                fila3[9] = f39;
                hora = (int)(Math.random()*30+30);
                if(hora + segA2 >= 60)
                {
                    minA2+=1;
                    segA2 = (hora + segA2) - 60;
                    if(minA2 >= 60)
                    {
                        hrA2+=1;
                        minA2 = 0;
                    }
                }
                else
                    segA2 = segA2 + hora;
                hora = Integer.parseInt(f38);
                if(hora + segA2 >= 60)
                {
                    minA2+=1;
                    segA2 = (hora + segA2) - 60;
                    if(minA2 >= 60)
                    {
                        hrA2+=1;
                        minA2 = 0;
                    }
                }
                else
                    segA2 += hora;
                String f310 = hrA2 + ":" + minA2 + ":" + segA2;  
                fila3[10] = f310;
                String f311 = (int)(Math.random()*20+21) + "";
                fila3[11] = f311;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 3 HIBS: " + hora);
                if(hora + segA2 >= 60)
                {
                    minA2+=1;
                    segA2 = (hora + segA2) - 60;
                    if(minA2 >= 60)
                    {
                        hrA2+=1;
                        minA2 = 0;
                    }
                }
                else
                    segA2 = segA2 + hora;
                hora = Integer.parseInt(f311);
                if(hora + segA2 >= 60)
                {
                    minA2+=1;
                    segA2 = (hora + segA2) - 60;
                    if(minA2 >= 60)
                    {
                        hrA2+=1;
                        minA2 = 0;
                    }
                }
                else
                    segA2 += hora;
                String f312 = hrA2 + ":" + minA2 + ":" + segA2;          
                fila3[12] = f312;
                String f313 = (int)(Math.random()*20+21) + "";
                fila3[13] = f313;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 3 HILS: " + hora);
                if(hora + segA2 >= 60)
                {
                    minA2+=1;
                    segA2 = (hora + segA2) - 60;
                    if(minA2 >= 60)
                    {
                        hrA2+=1;
                        minA2 = 0;
                    }
                }
                else
                    segA2 = segA2 + hora;
                hora = Integer.parseInt(f313);
                if(hora + segA2 >= 60)
                {
                    minA2+=1;
                    segA2 = (hora + segA2) - 60;
                    if(minA2 >= 60)
                    {
                        hrA2+=1;
                        minA2 = 0;
                    }
                }
                else
                    segA2 += hora;
                String f314 = hrA2 + ":" + minA2 + ":" + segA2;     
                fila3[14] = f314;
                String f315 = (int)(Math.random()*20+21) + "";
                fila3[15] = f315;
                tablaP.insertRow(2, fila3);
                
                
                fila4[0] = "004";                
                hora = (int)(Math.random()*16+30);
                //System.out.println("proceso 4"+hora);
                if(hora + segA3 >= 60)
                {
                    minA3+=1;
                    segA3 = (hora + segA3) - 60;
                    if(minA3 >= 60)
                    {
                        hrA3+=1;
                        minA3 = 0;
                    }
                }
                else
                    segA3 += hora;
                int hrA4 = hrA3, minA4 = minA3, segA4 = segA3;
                String f41 = hrA3 + ":" + minA3 + ":" + segA3;  
                fila4[1] = f41;
                String f42 = (int)(Math.random()*60+60) + "";
                fila4[2] = f42;
                fila4[3] = 5 + "";
                String f44 = (int)(Math.random()*11) + "";
                fila4[4] = f44;                
                fila4[5] ="Ejecución";
                String f46 = ((int)(Math.random()*4+2)*10) + "";
                fila4[6] = f46;
                hora = (int)(Math.random()*30+30);  
//                System.out.println("proceso 4: " + hora);
                if(hora + segA3 >= 60)
                {
                    minA3+=1;
                    segA3 = (hora + segA3) - 60;
                    if(minA3 >= 60)
                    {
                        hrA3+=1;
                        minA3 = 0;
                    }
                }
                else                                   
                    segA3 = segA3 + hora;
                String f47 = hrA3 + ":" + minA3 + ":" + segA3;  
                fila4[7] = f47;
                String f48 = (int)(Math.random()*20+20) + "";                
                fila4[8] = f48;
                String f49 = ((int)(Math.random()*4+3)*10) + "";
                fila4[9] = f49;
                hora = (int)(Math.random()*30+30);
                if(hora + segA3 >= 60)
                {
                    minA3+=1;
                    segA3 = (hora + segA3) - 60;
                    if(minA3 >= 60)
                    {
                        hrA3+=1;
                        minA3 = 0;
                    }
                }
                else
                    segA3 = segA3 + hora;
                hora = Integer.parseInt(f48);
                if(hora + segA3 >= 60)
                {
                    minA3+=1;
                    segA3 = (hora + segA3) - 60;
                    if(minA3 >= 60)
                    {
                        hrA3+=1;
                        minA3 = 0;
                    }
                }
                else
                    segA3 += hora;
                String f410 = hrA3 + ":" + minA3 + ":" + segA3;  
                fila4[10] = f410;
                String f411 = (int)(Math.random()*20+21) + "";
                fila4[11] = f411;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 4 HIBS: " + hora);
                if(hora + segA3 >= 60)
                {
                    minA3+=1;
                    segA3 = (hora + segA3) - 60;
                    if(minA3 >= 60)
                    {
                        hrA3+=1;
                        minA3 = 0;
                    }
                }
                else
                    segA3 = segA3 + hora;
                hora = Integer.parseInt(f411);
                if(hora + segA3 >= 60)
                {
                    minA3+=1;
                    segA3 = (hora + segA3) - 60;
                    if(minA3 >= 60)
                    {
                        hrA3+=1;
                        minA3 = 0;
                    }
                }
                else
                    segA3 += hora;
                String f412 = hrA3 + ":" + minA3 + ":" + segA3;                
                fila4[12] = f412;
                String f413 = (int)(Math.random()*20+21) + "";
                fila4[13] = f413;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 4 HILS: " + hora);
                if(hora + segA3 >= 60)
                {
                    minA3+=1;
                    segA3 = (hora + segA3) - 60;
                    if(minA3 >= 60)
                    {
                        hrA3+=1;
                        minA3 = 0;
                    }
                }
                else
                    segA3 = segA3 + hora;
                hora = Integer.parseInt(f413);
                if(hora + segA3 >= 60)
                {
                    minA3+=1;
                    segA3 = (hora + segA3) - 60;
                    if(minA3 >= 60)
                    {
                        hrA3+=1;
                        minA3 = 0;
                    }
                }
                else
                    segA3 += hora;
                String f414 = hrA3 + ":" + minA3 + ":" + segA3;  
                fila4[14] = f414;
                String f415 = (int)(Math.random()*20+21) + "";
                fila4[15] = f415;                
                tablaP.insertRow(3, fila4);
                
                
                fila5[0] = "005";
                hora = (int)(Math.random()*16+30);
                //System.out.println("proceso 5"+hora);
                if(hora + segA4 >= 60)
                {
                    minA4+=1;
                    segA4 = (hora + segA4) - 60;
                    if(minA4 >= 60)
                    {
                        hrA4+=1;
                        minA4 = 0;
                    }
                }
                else
                    segA4 += hora;
                int hrA5 = hrA4, minA5 = minA4, segA5 = segA4;
                String f51 = hrA4 + ":" + minA4 + ":" + segA4;  
                fila5[1] = f51;
                String f52 = (int)(Math.random()*60+60) + "";
                fila5[2] = f52;
                fila5[3] = 5 + "";
                String f54 = (int)(Math.random()*11) + "";
                fila5[4] = f54;                
                fila5[5] = "Ejecución";
                String f56 = ((int)(Math.random()*4+2)*10) + "";
                fila5[6] = f56;
                hora = (int)(Math.random()*30+30);
//                System.out.println("proceso 5: " + hora);                
                if(hora + segA4 >= 60)
                {
                    minA4+=1;
                    segA4 = (hora + segA4) - 60;
                    if(minA4 >= 60)
                    {
                        hrA4+=1;
                        minA4 = 0;
                    }
                }
                else                                   
                    segA4 = segA4 + hora;
                String f57 = hrA4 + ":" + minA4 + ":" + segA4;  
                fila5[7] = f57;
                String f58 = (int)(Math.random()*20+20) + "";                
                fila5[8] = f58;
                String f59 = ((int)(Math.random()*4+3)*10) + "";
                fila5[9] = f59;
                hora = (int)(Math.random()*30+30); 
                if(hora + segA4 >= 60)
                {
                    minA4+=1;
                    segA4 = (hora + segA4) - 60;
                    if(minA4 >= 60)
                    {
                        hrA4+=1;
                        minA4 = 0;
                    }
                }
                else
                    segA4 = segA4 + hora;
                hora = Integer.parseInt(f58);
                if(hora + segA4 >= 60)
                {
                    minA4+=1;
                    segA4 = (hora + segA4) - 60;
                    if(minA4 >= 60)
                    {
                        hrA4+=1;
                        minA4 = 0;
                    }
                }
                else
                    segA4 += hora;
                String f510 = hrA4 + ":" + minA4 + ":" + segA4;  
                fila5[10] = f510;
                String f511 = (int)(Math.random()*20+21) + "";
                fila5[11] = f511;
                hora = (int)(Math.random()*30+31); 
                System.out.println("proceso 5 HIBS: " + hora);
                if(hora + segA4 >= 60)
                {
                    minA4+=1;
                    segA4 = (hora + segA4) - 60;
                    if(minA4 >= 60)
                    {
                        hrA4+=1;
                        minA4 = 0;
                    }
                }
                else
                    segA4 = segA4 + hora;
                hora = Integer.parseInt(f511);
                if(hora + segA4 >= 60)
                {
                    minA4+=1;
                    segA4 = (hora + segA4) - 60;
                    if(minA4 >= 60)
                    {
                        hrA4+=1;
                        minA4 = 0;
                    }
                }
                else
                    segA4 += hora;
                String f512 = hrA4 + ":" + minA4 + ":" + segA4;             
                fila5[12] = f512;
                String f513 = (int)(Math.random()*20+21) + "";
                fila5[13] = f513;
                hora = (int)(Math.random()*30+31); 
                System.out.println("proceso 5 HILS: " + hora);
                if(hora + segA4 >= 60)
                {
                    minA4+=1;
                    segA4 = (hora + segA4) - 60;
                    if(minA4 >= 60)
                    {
                        hrA4+=1;
                        minA4 = 0;
                    }
                }
                else
                    segA4 = segA4 + hora;
                hora = Integer.parseInt(f513);
                if(hora + segA4 >= 60)
                {
                    minA4+=1;
                    segA4 = (hora + segA4) - 60;
                    if(minA4 >= 60)
                    {
                        hrA4+=1;
                        minA4 = 0;
                    }
                }
                else
                    segA4 += hora;
                String f514 = hrA4 + ":" + minA4 + ":" + segA4; 
                fila5[14] = f514;
                String f515 = (int)(Math.random()*20+21) + "";
                fila5[15] = f515;       
                tablaP.insertRow(4, fila5);
                
                
                fila6[0] = "006";
                hora = (int)(Math.random()*16+30);
                //System.out.println("proceso 6"+hora);
                if(hora + segA5 >= 60)
                {
                    minA5+=1;
                    segA5 = (hora + segA5) - 60;
                    if(minA5 >= 60)
                    {
                        hrA5+=1;
                        minA5 = 0;
                    }
                }
                else
                    segA5 += hora;
                int hrA6 = hrA5, minA6 = minA5, segA6 = segA5;
                String f61 = hrA5 + ":" + minA5 + ":" + segA5;  
                fila6[1] = f61;
                String f62 = (int)(Math.random()*60+60) + "";
                fila6[2] = f62;
                fila6[3] = 5 + "";
                String f64 = (int)(Math.random()*11) + "";
                fila6[4] = f64;                
                fila6[5] = "Ejecución";
                String f66 = ((int)(Math.random()*4+2)*10) + "";
                fila6[6] = f66;
                hora = (int)(Math.random()*30+30);
//                System.out.println("proceso 6: " + hora);                
                if(hora + segA5 >= 60)
                {
                    minA5+=1;
                    segA5 = (hora + segA5) - 60;
                    if(minA5 >= 60)
                    {
                        hrA5+=1;
                        minA5 = 0;
                    }
                }
                else                                   
                    segA5 = segA5 + hora;
                String f67 = hrA5 + ":" + minA5 + ":" + segA5;  
                fila6[7] = f67;
                String f68 = (int)(Math.random()*20+20) + "";                
                fila6[8] = f68;
                String f69 = ((int)(Math.random()*4+3)*10) + "";
                fila6[9] = f69;
                hora = (int)(Math.random()*30+30);
                if(hora + segA5 >= 60)
                {
                    minA5+=1;
                    segA5 = (hora + segA5) - 60;
                    if(minA5 >= 60)
                    {
                        hrA5+=1;
                        minA5 = 0;
                    }
                }
                else
                    segA5 = segA5 + hora;
                hora = Integer.parseInt(f68);
                if(hora + segA5 >= 60)
                {
                    minA5+=1;
                    segA5 = (hora + segA5) - 60;
                    if(minA5 >= 60)
                    {
                        hrA5+=1;
                        minA5 = 0;
                    }
                }
                else
                    segA5 += hora;
                String f610 = hrA5 + ":" + minA5 + ":" + segA5;  
                fila6[10] = f610;
                String f611 = (int)(Math.random()*20+21) + "";
                fila6[11] = f611;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 6 HIBS: " + hora);
                if(hora + segA5 >= 60)
                {
                    minA5+=1;
                    segA5 = (hora + segA5) - 60;
                    if(minA5 >= 60)
                    {
                        hrA5+=1;
                        minA5 = 0;
                    }
                }
                else
                    segA5 = segA5 + hora;
                hora = Integer.parseInt(f611);
                if(hora + segA5 >= 60)
                {
                    minA5+=1;
                    segA5 = (hora + segA5) - 60;
                    if(minA5 >= 60)
                    {
                        hrA5+=1;
                        minA5 = 0;
                    }
                }
                else
                    segA5 += hora;
                String f612 = hrA5 + ":" + minA5 + ":" + segA5;                
                fila6[12] = f612;
                String f613 = (int)(Math.random()*20+21) + "";
                fila6[13] = f613;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 6 HILS: " + hora);
                if(hora + segA5 >= 60)
                {
                    minA5+=1;
                    segA5 = (hora + segA5) - 60;
                    if(minA5 >= 60)
                    {
                        hrA5+=1;
                        minA5 = 0;
                    }
                }
                else
                    segA5 = segA5 + hora;
                hora = Integer.parseInt(f613);
                if(hora + segA5 >= 60)
                {
                    minA5+=1;
                    segA5 = (hora + segA5) - 60;
                    if(minA5 >= 60)
                    {
                        hrA5+=1;
                        minA5 = 0;
                    }
                }
                else
                    segA5 += hora;
                String f614 = hrA5 + ":" + minA5 + ":" + segA5;     
                fila6[14] = f614;
                String f615 = (int)(Math.random()*20+21) + "";
                fila6[15] = f615;                       
                tablaP.insertRow(5, fila6);
                
                
                fila7[0] = "007";
                hora = (int)(Math.random()*16+30);
                //System.out.println("proceso 7"+hora);
                if(hora + segA6 >= 60)
                {
                    minA6+=1;
                    segA6 = (hora + segA6) - 60;
                    if(minA6 >= 60)
                    {
                        hrA6+=1;
                        minA6 = 0;
                    }
                }
                else
                    segA6 += hora;
                int hrA7 = hrA6, minA7 = minA6, segA7 = segA6;
                String f71 = hrA6 + ":" + minA6 + ":" + segA6;  
                fila7[1] = f71;
                String f72 = (int)(Math.random()*60+60) + "";
                fila7[2] = f72;
                fila7[3] = 5 + "";
                String f74 = (int)(Math.random()*11) + "";
                fila7[4] = f74;                
                fila7[5] = "Ejecución";
                String f76 = ((int)(Math.random()*4+2)*10) + "";
                fila7[6] = f76;
                hora = (int)(Math.random()*30+30); 
//                System.out.println("proceso 7: " + hora);
                if(hora + segA6 >= 60)
                {
                    minA6+=1;
                    segA6 = (hora + segA6) - 60;
                    if(minA6 >= 60)
                    {
                        hrA6+=1;
                        minA6 = 0;
                    }
                }
                else                                   
                    segA6 = segA6 + hora;
                String f77 = hrA6 + ":" + minA6 + ":" + segA6;  
                fila7[7] = f77;
                String f78 = (int)(Math.random()*20+20) + "";                
                fila7[8] = f78;
                String f79 = ((int)(Math.random()*4+3)*10) + "";
                fila7[9] = f79;
                hora = (int)(Math.random()*30+30);
                if(hora + segA6 >= 60)
                {
                    minA6+=1;
                    segA6 = (hora + segA6) - 60;
                    if(minA6 >= 60)
                    {
                        hrA6+=1;
                        minA6 = 0;
                    }
                }
                else
                    segA6 = segA6 + hora;
                hora = Integer.parseInt(f78);
                if(hora + segA6 >= 60)
                {
                    minA6+=1;
                    segA6 = (hora + segA6) - 60;
                    if(minA6 >= 60)
                    {
                        hrA6+=1;
                        minA6 = 0;
                    }
                }
                else
                    segA6 += hora;
                String f710 = hrA6 + ":" + minA6 + ":" + segA6;  
                fila7[10] = f710;
                String f711 = (int)(Math.random()*20+21) + "";
                fila7[11] = f711;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 7 HIBS: " + hora);
                if(hora + segA6 >= 60)
                {
                    minA6+=1;
                    segA6 = (hora + segA6) - 60;
                    if(minA6 >= 60)
                    {
                        hrA6+=1;
                        minA6 = 0;
                    }
                }
                else
                    segA6 = segA6 + hora;
                hora = Integer.parseInt(f711);
                if(hora + segA6 >= 60)
                {
                    minA6+=1;
                    segA6 = (hora + segA6) - 60;
                    if(minA6 >= 60)
                    {
                        hrA6+=1;
                        minA6 = 0;
                    }
                }
                else
                    segA6 += hora;
                String f712 = hrA6 + ":" + minA6 + ":" + segA6;             
                fila7[12] = f712;
                String f713 = (int)(Math.random()*20+21) + "";
                fila7[13] = f713;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 7 HILS: " + hora);
                if(hora + segA6 >= 60)
                {
                    minA6+=1;
                    segA6 = (hora + segA6) - 60;
                    if(minA6 >= 60)
                    {
                        hrA6+=1;
                        minA6 = 0;
                    }
                }
                else
                    segA6 = segA6 + hora;
                hora = Integer.parseInt(f713);
                if(hora + segA6 >= 60)
                {
                    minA6+=1;
                    segA6 = (hora + segA6) - 60;
                    if(minA6 >= 60)
                    {
                        hrA6+=1;
                        minA6 = 0;
                    }
                }
                else
                    segA6 += hora;
                String f714 = hrA6 + ":" + minA6 + ":" + segA6; 
                fila7[14] = f714;
                String f715 = (int)(Math.random()*20+21) + "";
                fila7[15] = f715; 
                tablaP.insertRow(6, fila7);
                
                
                fila8[0] = "008";
                hora = (int)(Math.random()*16+30);
                //System.out.println("proceso 8"+hora);
                if(hora + segA7 >= 60)
                {
                    minA7+=1;
                    segA7 = (hora + segA7) - 60;
                    if(minA7 >= 60)
                    {
                        hrA7+=1;
                        minA7 = 0;
                    }
                }
                else
                    segA7 += hora;
                int hrA8 = hrA7, minA8 = minA7, segA8 = segA7;
                String f81 = hrA7 + ":" + minA7 + ":" + segA7;  
                fila8[1] = f81;
                String f82 = (int)(Math.random()*60+60) + "";
                fila8[2] = f82;
                fila8[3] = 5 + "";
                String f84 = (int)(Math.random()*11) + "";
                fila8[4] = f84;                
                fila8[5] = "Ejecución";
                String f86 = ((int)(Math.random()*4+2)*10) + "";
                fila8[6] = f86;
                hora = (int)(Math.random()*30+30);
//                System.out.println("proceso 8: " + hora);                
                if(hora + segA7 >= 60)
                {
                    minA7+=1;
                    segA7 = (hora + segA7) - 60;
                    if(minA7 >= 60)
                    {
                        hrA7+=1;
                        minA7 = 0;
                    }
                }
                else                                   
                    segA7 = segA7 + hora;
                String f87 = hrA7 + ":" + minA7 + ":" + segA7;  
                fila8[7] = f87;
                String f88 = (int)(Math.random()*20+20) + "";                
                fila8[8] = f88;
                String f89 = ((int)(Math.random()*4+3)*10) + "";
                fila8[9] = f89;
                hora = (int)(Math.random()*30+30);
                if(hora + segA7 >= 60)
                {
                    minA7+=1;
                    segA7 = (hora + segA7) - 60;
                    if(minA7 >= 60)
                    {
                        hrA7+=1;
                        minA7 = 0;
                    }
                }
                else
                    segA7 = segA7 + hora;
                hora = Integer.parseInt(f88);
                if(hora + segA7 >= 60)
                {
                    minA7+=1;
                    segA7 = (hora + segA7) - 60;
                    if(minA7 >= 60)
                    {
                        hrA7+=1;
                        minA7 = 0;
                    }
                }
                else
                    segA7 += hora;
                String f810 = hrA7 + ":" + minA7 + ":" + segA7;  
                fila8[10] = f810;
                String f811 = (int)(Math.random()*20+21) + "";
                fila8[11] = f811;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 8 HIBS: " + hora);
                if(hora + segA7 >= 60)
                {
                    minA7+=1;
                    segA7 = (hora + segA7) - 60;
                    if(minA7 >= 60)
                    {
                        hrA7+=1;
                        minA7 = 0;
                    }
                }
                else
                    segA7 = segA7 + hora;
                hora = Integer.parseInt(f811);
                if(hora + segA7 >= 60)
                {
                    minA7+=1;
                    segA7 = (hora + segA7) - 60;
                    if(minA7 >= 60)
                    {
                        hrA7+=1;
                        minA7 = 0;
                    }
                }
                else
                    segA7 += hora;
                String f812 = hrA7 + ":" + minA7 + ":" + segA7;               
                fila8[12] = f812;
                String f813 = (int)(Math.random()*20+21) + "";
                fila8[13] = f813;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 8 HILS: " + hora);
                if(hora + segA7 >= 60)
                {
                    minA7+=1;
                    segA7 = (hora + segA7) - 60;
                    if(minA7 >= 60)
                    {
                        hrA7+=1;
                        minA7 = 0;
                    }
                }
                else
                    segA7 = segA7 + hora;
                hora = Integer.parseInt(f813);
                if(hora + segA7 >= 60)
                {
                    minA7+=1;
                    segA7 = (hora + segA7) - 60;
                    if(minA7 >= 60)
                    {
                        hrA7+=1;
                        minA7 = 0;
                    }
                }
                else
                    segA7 += hora;
                String f814 = hrA7 + ":" + minA7 + ":" + segA7;   
                fila8[14] = f814;
                String f815 = (int)(Math.random()*20+21) + "";
                fila8[15] = f815; 
                tablaP.insertRow(7, fila8);
                
                
                fila9[0] = "009";
                hora = (int)(Math.random()*16+30);
                //System.out.println("proceso 9"+hora);
                if(hora + segA8 >= 60)
                {
                    minA8+=1;
                    segA8 = (hora + segA8) - 60;
                    if(minA8 >= 60)
                    {
                        hrA8+=1;
                        minA8 = 0;
                    }
                }
                else
                    segA8 += hora;
                int hrA9 = hrA8, minA9 = minA8, segA9 = segA8;
                String f91 = hrA8 + ":" + minA8 + ":" + segA8;  
                fila9[1] = f91;
                String f92 = (int)(Math.random()*60+60) + "";
                fila9[2] = f92;
                fila9[3] = 5 + "";
                String f94 = (int)(Math.random()*11) + "";
                fila9[4] = f94;                
                fila9[5] = "Ejecución";
                String f96 = ((int)(Math.random()*4+2)*10) + "";
                fila9[6] = f96;
                hora = (int)(Math.random()*30+30);
//                System.out.println("proceso 9: " + hora);                
                if(hora + segA8 >= 60)
                {
                    minA8+=1;
                    segA8 = (hora + segA8) - 60;
                    if(minA8 >= 60)
                    {
                        hrA8+=1;
                        minA8 = 0;
                    }
                }
                else                                   
                    segA8 = segA8 + hora;
                String f97 = hrA8 + ":" + minA8 + ":" + segA8;  
                fila9[7] = f97;
                String f98 = (int)(Math.random()*20+20) + "";                
                fila9[8] = f98;
                String f99 = ((int)(Math.random()*4+3)*10) + "";
                fila9[9] = f99;
                hora = (int)(Math.random()*30+30);
                if(hora + segA8 >= 60)
                {
                    minA8+=1;
                    segA8 = (hora + segA8) - 60;
                    if(minA8 >= 60)
                    {
                        hrA8+=1;
                        minA8 = 0;
                    }
                }
                else
                    segA8 = segA8 + hora;
                hora = Integer.parseInt(f98);
                if(hora + segA8 >= 60)
                {
                    minA8+=1;
                    segA8 = (hora + segA8) - 60;
                    if(minA8 >= 60)
                    {
                        hrA8+=1;
                        minA8 = 0;
                    }
                }
                else
                    segA8 += hora;
                String f910 = hrA8 + ":" + minA8 + ":" + segA8;  
                fila9[10] = f910;
                String f911 = (int)(Math.random()*20+21) + "";
                fila9[11] = f911;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 9 HIBS: " + hora);
                if(hora + segA8 >= 60)
                {
                    minA8+=1;
                    segA8 = (hora + segA8) - 60;
                    if(minA8 >= 60)
                    {
                        hrA8+=1;
                        minA8 = 0;
                    }
                }
                else
                    segA8 = segA8 + hora;
                hora = Integer.parseInt(f911);
                if(hora + segA8 >= 60)
                {
                    minA8+=1;
                    segA8 = (hora + segA8) - 60;
                    if(minA8 >= 60)
                    {
                        hrA8+=1;
                        minA8 = 0;
                    }
                }
                else
                    segA8 += hora;
                String f912 = hrA8 + ":" + minA8 + ":" + segA8;               
                fila9[12] = f912;
                String f913 = (int)(Math.random()*20+21) + "";
                fila9[13] = f913;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 9 HILS: " + hora);
                if(hora + segA8 >= 60)
                {
                    minA8+=1;
                    segA8 = (hora + segA8) - 60;
                    if(minA8 >= 60)
                    {
                        hrA8+=1;
                        minA8 = 0;
                    }
                }
                else
                    segA8 = segA8 + hora;
                hora = Integer.parseInt(f913);
                if(hora + segA8 >= 60)
                {
                    minA8+=1;
                    segA8 = (hora + segA8) - 60;
                    if(minA8 >= 60)
                    {
                        hrA8+=1;
                        minA8 = 0;
                    }
                }
                else
                    segA8 += hora;
                String f914 = hrA8 + ":" + minA8 + ":" + segA8;   
                fila9[14] = f914;
                String f915 = (int)(Math.random()*20+21) + "";
                fila9[15] = f915;
                tablaP.insertRow(8, fila9);
                
                
                fila10[0] = "010";
                hora = (int)(Math.random()*16+30);
                //System.out.println("proceso 10"+hora);
                if(hora + segA9 >= 60)
                {
                    minA9+=1;
                    segA9 = (hora + segA9) - 60;
                    if(minA9 >= 60)
                    {
                        hrA9+=1;
                        minA9 = 0;
                    }
                }
                else
                    segA9 += hora;
                String f101 = hrA9 + ":" + minA9 + ":" + segA9;  
                fila10[1] = f101;
                String f102 = (int)(Math.random()*60+60) + "";
                fila10[2] = f102;
                fila10[3] = 5 + "";
                String f104 = (int)(Math.random()*11) + "";
                fila10[4] = f104;                
                fila10[5] = "Ejecución";
                String f106 = ((int)(Math.random()*4+2)*10) + "";
                fila10[6] = f106;
                hora = (int)(Math.random()*30+30); 
//                System.out.println("proceso 10: " + hora);
                if(hora + segA9 >= 60)
                {
                    minA9+=1;
                    segA9 = (hora + segA9) - 60;
                    if(minA9 >= 60)
                    {
                        hrA9+=1;
                        minA9 = 0;
                    }
                }
                else                                   
                    segA9 = segA9 + hora;
                String f107 = hrA9 + ":" + minA9 + ":" + segA9;  
                fila10[7] = f107;
                String f108 = (int)(Math.random()*20+20) + "";                
                fila10[8] = f108;
                String f109 = ((int)(Math.random()*4+3)*10) + "";
                fila10[9] = f109;
                hora = (int)(Math.random()*30+30);
                if(hora + segA9 >= 60)
                {
                    minA9+=1;
                    segA9 = (hora + segA9) - 60;
                    if(minA9 >= 60)
                    {
                        hrA9+=1;
                        minA9 = 0;
                    }
                }
                else
                    segA9 = segA9 + hora;
                hora = Integer.parseInt(f108);
                if(hora + segA9 >= 60)
                {
                    minA9+=1;
                    segA9 = (hora + segA9) - 60;
                    if(minA9 >= 60)
                    {
                        hrA9+=1;
                        minA9 = 0;
                    }
                }
                else
                    segA9 += hora;
                String f1010 = hrA9 + ":" + minA9 + ":" + segA9;  
                fila10[10] = f1010;
                String f1011 = (int)(Math.random()*20+21) + "";
                fila10[11] = f1011;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 10 HIBS: " + hora);
                if(hora + segA9 >= 60)
                {
                    minA9+=1;
                    segA9 = (hora + segA9) - 60;
                    if(minA9 >= 60)
                    {
                        hrA9+=1;
                        minA9 = 0;
                    }
                }
                else
                    segA9 = segA9 + hora;
                hora = Integer.parseInt(f1011);
                if(hora + segA9 >= 60)
                {
                    minA9+=1;
                    segA9 = (hora + segA9) - 60;
                    if(minA9 >= 60)
                    {
                        hrA9+=1;
                        minA9 = 0;
                    }
                }
                else
                    segA9 += hora;
                String f1012 = hrA9 + ":" + minA9 + ":" + segA9;               
                fila10[12] = f1012;
                String f1013 = (int)(Math.random()*20+21) + "";
                fila10[13] = f1013;
                hora = (int)(Math.random()*30+31);
                System.out.println("proceso 10 HILS: " + hora);
                if(hora + segA9 >= 60)
                {
                    minA9+=1;
                    segA9 = (hora + segA9) - 60;
                    if(minA9 >= 60)
                    {
                        hrA9+=1;
                        minA9 = 0;
                    }
                }
                else
                    segA9 = segA9 + hora;
                hora = Integer.parseInt(f1013);
                if(hora + segA9 >= 60)
                {
                    minA9+=1;
                    segA9 = (hora + segA9) - 60;
                    if(minA9 >= 60)
                    {
                        hrA9+=1;
                        minA9 = 0;
                    }
                }
                else
                    segA9 += hora;
                String f1014 = hrA9 + ":" + minA9 + ":" + segA9;  
                fila10[14] = f1014;
                String f1015 = (int)(Math.random()*20+21) + "";
                fila10[15] = f1015;
                tablaP.insertRow(9, fila10);
            }
        });
    }
    
}
